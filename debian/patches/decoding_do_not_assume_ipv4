Description: Do not assume IPv4 packets when decoding
 When using Snort on a interface without a link level layer, for example a
 AIYIA tunnel for IPv6 through SixXs, then snort assumes that the packets will
 be IPv4. I have a patch that adds a check on the IP version number in the
 header, and if it is not an IPv4 packet, try decoding as IPv6.
.
 Without this patch, listening on such an interface, when IPv6 traffic is
 seen will result in warning messages as below:
 Not IPv4 datagram! ([ver: 0x6][len: 0x0])
Author: Hugh Davenport <hugh@davenport.net.nz>
Reviewed-by: Javier Fernandez-Sanguino <jfs@debian.org>
Origin: vendor
Bug: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=633066
Forwarded: no
Last-Update: 2020-12-06


--- a/src/decode.c
+++ b/src/decode.c
@@ -2077,7 +2077,7 @@ void DecodePppPktEncapsulated(const uint
  *
  * Purpose: Decodes packets coming in raw on layer 2, like PPP.  Coded and
  *          in by Jed Pickle (thanks Jed!) and modified for a few little tweaks
- *          by me.
+ *          by me, and by Hugh Davenport.
  *
  * Arguments: p => pointer to decoded packet struct
  *            pkthdr => ptr to the packet header
@@ -2098,9 +2098,17 @@ void DecodeRawPkt(Packet * p, const DAQ_
     p->pkth = pkthdr;
     p->pkt = pkt;
 
-    DEBUG_WRAP(DebugMessage(DEBUG_DECODE, "Raw IP4 Packet!\n"););
 
-    DecodeIP(pkt, p->pkth->caplen, p);
+    if (IP_VER((IPHdr *)pkt) == 4) 
+    {
+        DEBUG_WRAP(DebugMessage(DEBUG_DECODE, "Raw IP4 Packet!\n"););
+        DecodeIP(pkt, p->pkth->caplen, p);
+    } 
+    else
+    {
+        DEBUG_WRAP(DebugMessage(DEBUG_DECODE, "Raw IP6 Packet!\n"););
+        DecodeIPV6(pkt, p->pkth->caplen, p);
+    }
 
     PREPROC_PROFILE_END(decodePerfStats);
     return;
